"use strict";

// VARIABLES
var autoprefixerList = [
        'Chrome >= 45',
        'Firefox ESR',
        'Edge >= 12',
        'Explorer >= 10',
        'iOS >= 9',
        'Safari >= 9',
        'Android >= 4.4',
        'Opera >= 30'
    ],
    path = {
        build: {
            html: './',
            js: 'js/',
            css: 'css/',
            img: 'img/',
            fonts: 'fonts/'
        },
        src: {
            html: 'src/*.html',
            js: 'src/js/main.js',
            jsPlugins: 'src/js/_libs/*.js',
            style: 'src/scss/style.scss',
            stylePlugins: 'src/scss/_libs/*.scss',
            img: 'src/img/**/*',
            pngSpriteIcons: 'src/img/icons_sprite_png/*.png',
            fonts: 'src/fonts/**/*'
        },
        watch: {
            html: 'src/**/*.html',
            js: ['src/js/**/*.js', '!src/js/_libs/'],
            jsPlugins: 'src/js/_libs/*.js',
            css: ['src/scss/**/*.scss', '!src/scss/_libs/'],
            cssPlugins: 'src/scss/_libs/*.scss',
            img: 'src/img/**/*',
            pngSpriteIcons: 'src/img/icons_sprite_png/*.png',
            fonts: 'srs/fonts/**/*'
        },
        clean: 'js/, css/, img/, fonts/'
    },
    gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    rigger = require('gulp-rigger'),
    concat = require('gulp-concat'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    uglify = require('gulp-uglify'),
    cache = require('gulp-cache'),
    imagemin = require('gulp-imagemin'),
    jpegrecompress = require('imagemin-jpeg-recompress'),
    pngquant = require('imagemin-pngquant'),
    del = require('del'),
    babel = require('gulp-babel'),
    rename = require('gulp-rename');

function buildHtml() {
    return gulp
        .src( path.src.html )
        .pipe( rigger() )
        .pipe( plumber() )
        .pipe( gulp.dest( path.build.html ) );
}

function buildCss() {
    return gulp
        .src( path.src.style )
        .pipe( plumber() )
        .pipe( sourcemaps.init() )
        .pipe( sass() )
        .pipe( autoprefixer( {
            browsers: autoprefixerList
        } ) )
        .pipe( cleanCSS() )
        .pipe( sourcemaps.write( './' ) )
        .pipe( gulp.dest( path.build.css ) );
}

function buildCssPlugins() {
    return gulp
        .src( path.src.stylePlugins )
        .pipe( plumber() )
        .pipe( sourcemaps.init() )
        .pipe( concat( 'jquery-plugins.scss' ) )
        .pipe( sass() )
        .pipe( autoprefixer( {
            browsers: autoprefixerList
        } ) )
        .pipe( cleanCSS() )
        .pipe( sourcemaps.write( './' ) )
        .pipe( gulp.dest( path.build.css ) );
}

function buildJs() {
    return gulp
        .src( path.src.js )
        .pipe( plumber() )
        .pipe( sourcemaps.init() )
        .pipe( babel() )
        .pipe( uglify() )
        .pipe( sourcemaps.write( './' ) )
        .pipe( gulp.dest( path.build.js ) );
}

function buildJsPlugins() {
    return gulp
        .src( path.src.jsPlugins )
        .pipe( plumber() )
        .pipe( sourcemaps.init() )
        .pipe( concat( 'jquery-plugins.js' ) )
        .pipe( babel() )
        .pipe( uglify() )
        .pipe( sourcemaps.write( './' ) )
        .pipe( gulp.dest( path.build.js ) );
}

function buildImages() {
    return gulp
        .src( path.src.img )
        .pipe( newer( path.build.img ) )
        .pipe(
            imagemin( [
                imagemin.gifsicle( { interlaced: true } ),
                imagemin.jpegtran( { progressive: true } ),
                imagemin.optipng( { optimizationLevel: 5 } ),
                imagemin.svgo( {
                    plugins: [
                        {
                            removeViewBox: false,
                            collapseGroups: true
                        }
                    ]
                } )
            ] )
        )
        .pipe( gulp.dest( path.build.img ) );
}

function buildFonts() {
    return gulp
        .src( path.src.fonts )
        .pipe( gulp.dest( path.build.fonts ) );
}

function buildClean() {
    return del( [ path.clean ] );
}

function watchFiles() {
    gulp.watch( path.watch.html, buildHtml );
    gulp.watch( path.watch.css, buildCss );
    gulp.watch( path.watch.cssPlugins, buildCssPlugins );
    gulp.watch( path.watch.js, buildJs );
    gulp.watch( path.watch.jsPlugins, buildJsPlugins );
    gulp.watch( path.watch.img, buildImages );
    gulp.watch( path.watch.fonts, buildFonts );
}

exports.build = gulp.series( buildClean, gulp.parallel( buildHtml, buildCss, buildCssPlugins, buildJs, buildJsPlugins, buildImages, buildFonts ) );
exports.default = gulp.parallel( watchFiles );